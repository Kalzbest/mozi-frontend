# Mozi

mozi app frontend developed with [Angular2 framework](https://angular.io/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

1. Node.js v6.0 and above
2. [mozi-backend](https://github.com/meharig/mozi-back "Mozi-backend")

### Installing

1. clone or download this repository
2. open terminal and change directory to this folder
    ```
    cd ~/path/to/cloned/folder/frontend
    ```
3.  Install dependencies
    ```
    npm install
    ```
4. [start mozi-backend] (https://github.com/meharig/mozi-back "Mozi-backend")
5. start the application
    ```
    npm start
    ```
6. explore at [localhost:4200](http://localhost:4200) on browser
    
## Authors

* **Kaleab Yitbarek**
* **Tsadkan Yitbarek**
