/**
 * Created by kal on 11/7/16.
 */

export interface Project {
  id?: string;
  title?: string;
  description?: string;
  dataSetFileName?: string;
  targetFeature: string;
  w1: boolean;
  isProcessed?: boolean;
  resultFileName?: string;
  isAnalyzed?:boolean;
}
