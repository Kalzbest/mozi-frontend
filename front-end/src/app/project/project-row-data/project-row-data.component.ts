import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'project-row-data',
  templateUrl: './project-row-data.component.html',
  styleUrls: ['./project-row-data.component.css']
})
export class ProjectRowDataComponent implements OnInit {
  @Input() id;
  @Input() from;
  @Input() size;

  constructor() { }

  ngOnInit() { 
  }

  @Output() analyse = new EventEmitter();

  OnAnalyse() {
    this.analyse.emit({})
  }

}
