/**
 * Created by kal on 11/7/16.
 */

import  { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class ProjectService {

  private url = "http://0.0.0.0:7070/api/Projects";

  constructor (private http: Http) {

  }

  private getJson(res: Response) {
    return res.json();
  }

  getProjects() {
    return this.http.get(this.url)
      .map(this.getJson);
  }

  getProject(id) {
    return this.http.get(this.url + "/" + id)
      .map(this.getJson);
  }

  addProject(project) {
    return this.http.post(this.url, project)
      .map(this.getJson);
  }

  removeProject(id) {
    return this.http.delete(this.url + "/" + id)
      .map(this.getJson)
  }


  analyze(id: string) {
    return this.http.get(this.url + "/" + id + '/startAnalyze').map(this.getJson);
  }

  getProjectResult(resultFileName: string) {
    return this.http.get("http://0.0.0.0:7070/api/Containers/results/download/"+resultFileName);
  }

  getProjectDataset(dataSetFileName: string) {
    return this.http.get("http://0.0.0.0:7070/api/Containers/datasets/download/"+dataSetFileName);

  }
}
