import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/Rx';


export interface State {
  url: String
}

const defaultState = {
  url: ''
}

const store = new BehaviorSubject<State>(defaultState);


@Injectable()
export class Store {
  private store = store;

 changes = this.store.asObservable().distinctUntilChanged();
 

 setState(state) {
  this.store.next(state);
 }

 getState(): State {
   return this.store.value;
 }

 purge() {
   this.store.next(defaultState);
 }

}
