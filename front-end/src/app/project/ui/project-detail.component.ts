/**
 * Created by kal on 11/16/16.
 */

import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, Params} from '@angular/router';
import 'rxjs/add/operator/switchMap';

import {ToastsManager} from "ng2-toastr/ng2-toastr";

import {ProjectService} from "../services/project.service";
import {Project} from "../models/project";
import {Response} from "@angular/http";

@Component({
    selector: 'project-detail',
    templateUrl: '../templates/project-detail.component.html'
})

export class ProjectDetailComponent implements OnInit {
    private accordions: Array<any> = [
     { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
     { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true },
     { title: 'Dynamic Title 3', content: 'Dynamic content 3', active: true }
   ];
   multiple: boolean = false;
    
    
    private from = 1;
    private size = 50;

    private row_data_page_selected = true;
    private visualize_result_page_selected = false;
    private id;
    private project: Project;
    private resultCombo:String;
    private dataset;
    private isCollapsed: boolean = false;
    private isAnalysed: boolean = false;


    constructor(private route: ActivatedRoute, private router: Router, private projectService: ProjectService, private toastr: ToastsManager,) {

    }

    ngOnInit(): void {
         this.route.params.subscribe(params => {
            this.id = params['id'];
        });
        this.route.params
            .map(params => params['id'])
            .switchMap(id => this.projectService.getProject(id))
            .subscribe(project => {
                this.project = project;
            }, error => {
            });

       
    }

    startAnalyse() {
        this.projectService
            .analyze(this.project.id)
            .subscribe(res => {
                this.isAnalysed = true;
                this.toastr.success('Analysis Started!', 'Success!');
                console.log(res);
            });
    }

    downloadResult(){
        this.projectService
            .getProjectResult(this.project.resultFileName)
            .subscribe(res=>{
                this.resultCombo =res.text()
            });

    }

    rowDataPageSelected() {
        this.row_data_page_selected = true;
        this.visualize_result_page_selected = false;
    }

    visualizeResultPageSelected() {
        this.row_data_page_selected = false;
        this.visualize_result_page_selected = true;
        if (this.isAnalysed) {
            this.downloadResult();
        }
    }

    debug() {
        console.log("hello");
    }
}
