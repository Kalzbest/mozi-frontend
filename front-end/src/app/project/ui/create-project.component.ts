import {Component, NgZone, Inject} from "@angular/core"
import {ProjectService} from "../services/project.service";
import {Project} from "../models/project";
import {ToastsManager} from "ng2-toastr/ng2-toastr";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {NgUploaderOptions} from "ngx-uploader";

@Component({
    selector: 'create-project',
    templateUrl: '../templates/create-project.component.html',
    providers: [ProjectService]
})
export class CreateProjectComponent {

    private projectForm: FormGroup;
    private project: Project = {
        "title": "",
        "description": "",
        "targetFeature": "",
        "w1": false
    };
    options: NgUploaderOptions;
    uploadResponse: any;

    constructor(private projectService: ProjectService, private fb: FormBuilder,
                private router: Router,
                private toastr: ToastsManager,
                @Inject(NgZone) private zone: NgZone) {

        this.options = new NgUploaderOptions({
            url: 'http://0.0.0.0:7070/api/Containers/datasets/upload',
            autoUpload: true,
            calculateSpeed: true
        });

        this.projectForm = this.fb.group({
            title: this.fb.control(null),
            description: this.fb.control(null),
            fileName: this.fb.control(null),
            targetFeature: this.fb.control(null),
            w1: this.fb.control(null)
        });
    }


    onSubmit() {
        if (this.uploadResponse) {
            this.project.dataSetFileName = this.uploadResponse.result.files.file[0].name;
        }
        this.projectService.addProject(this.project)
            .subscribe(res => {
                this.toastr.success('Project Created Successfully !', 'Success!');
                this.router.navigate(['projects']);
                this.resetForm();
            })
    }

    resetForm() {
        this.project = {
            "title": "",
            "description": "",
            "targetFeature": "",
            "w1": false
        };
    }

    handleUpload(data: any) {
        setTimeout(() => {
            this.zone.run(() => {
                if (data && data.response) {
                    this.uploadResponse = JSON.parse(data.response);
                }
            });
        });
    }

    onW1Changed (event) {
        this.project.w1 = event.checked;
    }

}
