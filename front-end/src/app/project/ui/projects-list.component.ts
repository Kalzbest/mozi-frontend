
/**
 * Created by kal on 11/16/16.
 */

import { Component, OnInit } from "@angular/core";

import {ProjectService} from "../services/project.service";
import {Project} from "../models/project";

@Component({
  selector: 'projects-list',
  templateUrl: '../templates/projects-list.component.html',
  styleUrls: ['../styles/project-list.component.css']
})

export class ProjectsListComponent implements OnInit{

  private projects: Project[];
  private randSize = Math.floor(Math.random() * 36) + 1;

  constructor (private projectService: ProjectService) {

  }

  ngOnInit() {
    this.projectService.getProjects()
      .subscribe(res => {
        this.projects = res;
        this.projects = this.projects.reverse()
      });
  }

  deleteProject(item){
      this.projectService
          .removeProject(item.id)
          .subscribe(res=>{
            if (res.count) {
              let index = this.projects.indexOf(item);
              this.projects.splice(index, 1);
            }
          });
  }

}
