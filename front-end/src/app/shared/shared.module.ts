/**
 * Created by tsadik on 2/25/17.
 */
import { NgModule } from '@angular/core';

import { ScrollableTableModule } from './scrollable-table/scrollable-table.module';

@NgModule({
  imports:[ScrollableTableModule],
  declarations: [

  ],
  providers:[],
  exports: [

  ]
})
export class SharedModule{}
